У нас возникла ситуация, когда все коммиты отправлены в `main`, хотя хотелось, 
чтобы была отдельная ветка `develop`.


Чтобы исправить ситуацию мы:

0. Клонируем репозиторий или подтягиваем изменения
	```bash
	git clone ...
	```
	или
	```bash
	git pull
	```
1. Создаём orphan ветку `develop`, указаывающую на `initial commit`
	```bash
	git checkout --orphan develop <initial commit hash>
	git commit -m "dev init"
	```
2. Делаем `cherry-pick` всех коммитов из `main`, которым там не место, и пушим
	```bash
	git cherry-pick <bad commit_1 hash>
	...
	git cherry-pick <bad commit_n hash>
	git push --set-upstream origin develop
	```
3. Возвращаемся в `main` и через интерактивный режим rebase удаляем лишние коммиты
	```bash
	git checkout main
	git rebase -i <initial commit hash>
	# удаляем коммиты в интерактивном режиме
	git push origin -f # если ветка protected, следует временно разрешить force-push
	```